package com.bank.chequescanner.test.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bank.chequescanner.test.dao.ChequeDAO;
import com.bank.chequescanner.test.model.Cheque;
import com.bank.chequescanner.test.repository.ChequeDataRepository;


@RestController
public class ChequeController {
	private final ChequeDataRepository chequeDataRepository;

	public ChequeController(ChequeDataRepository chequeDataRepository) {
		this.chequeDataRepository = chequeDataRepository;
	}

	@RequestMapping(value = "/fetchData", produces = "application/json", method = RequestMethod.GET)
	public List<Cheque> getChequeList() {
		return chequeDataRepository.findAll();
	}

	@RequestMapping(value = "/fetchData/{id}", produces = "application/json", method = RequestMethod.GET)
	public ResponseEntity<Cheque> getChequeDataById(@PathVariable("id") String id) {
		java.util.Optional<Cheque> chequeData = chequeDataRepository.findById(id);

	  if (chequeData.isPresent()) {
	    return new ResponseEntity<>(chequeData.get(), HttpStatus.OK);
	  } else {
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	  }
	}
	
	@RequestMapping(value = "/deleteMetaData/{id}", produces = "application/json", method = RequestMethod.DELETE)
	public ResponseEntity<HttpStatus> deleteChequeMetaData(@PathVariable("id") String id) {
	  try {
		  chequeDataRepository.deleteById(id);
	    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	  } catch (Exception e) {
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	  }
	}
	
	@RequestMapping(value = "/ping", produces = MediaType.TEXT_PLAIN_VALUE, method = RequestMethod.GET)
	public String ping() {
		return "pong";
	}

	@Autowired
	ChequeService chequeService;
	@RequestMapping(value = "/saveData", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE , MediaType.APPLICATION_JSON_VALUE}, produces = "application/json", method = RequestMethod.POST)
	public Cheque saveData(@RequestParam("file") MultipartFile file, @RequestParam("chequeMetaData") String chequeMetaData) throws IOException {
		
		Cheque chequeDataJson = chequeService.getJson(file, chequeMetaData);
		return chequeDataRepository.save(chequeDataJson);

	}
	
	@Autowired
	ChequeService chequeMetaDataService;
	@RequestMapping(value = "/saveChequeData", consumes = MediaType.MULTIPART_FORM_DATA_VALUE , produces = "application/json", method = RequestMethod.POST)
	public Cheque saveChequeData(@RequestParam("file") MultipartFile file) throws IOException {
		String chequeMetaData = "{\n"
				+ "\"date_Of_the_Cheque\": \"9 April 2021\",\n"
				+ "\"payee_Name\": \"Alex T\",\n"
				+ "\"amount_In_Figure\": \"100.00\",\n"
				+ "\"amount_In_Words\": \"Hundred only\",\n"
				+ "\"sort_Code\": \"054576\",\n"
				+ "\"bank_Name\": \"HSBC UK\",\n"
				+ "\"signature_Availability\": \"Yes\",\n"
				+ "\"cheque_Number\": \"4521547\",\n"
				+ "\"payers_Account_Number\": \"7438907481\",\n"
				+ "\"split_Details\": \"NA\"\n"
				+ "}\n"
				+ "";
		Cheque chequeDataJson = chequeMetaDataService.getJson(file, chequeMetaData);
		return chequeDataRepository.save(chequeDataJson);

	}

	
}