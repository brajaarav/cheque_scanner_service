package com.bank.chequescanner.test.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.bank.chequescanner.test.model.Cheque;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ChequeService {
	
	 private AmazonS3 s3client;

	    @Value("${amazonProperties.endpointUrl}")
	    private String endpointUrl;
	    @Value("${amazonProperties.bucketName}")
	    private String bucketName;
	    @Value("${amazonProperties.accessKey}")
	    private String accessKey;
	    @Value("${amazonProperties.secretKey}")
	    private String secretKey;

	    @PostConstruct
	    private void initializeAmazon() {
	        AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
	        this.s3client = new AmazonS3Client(credentials);
	    }
	 private static final Logger LOGGER = LoggerFactory.getLogger(ChequeService.class);
	 
	
	public Cheque getJson(MultipartFile file, String chequeMetaData)
	{
		 LOGGER.info("File upload is in progress.");
		 LOGGER.info("File received ----->"+file.getOriginalFilename());
		 
		 uploadFile(file);
		 
		/*String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		String path = "/Users/brajbhooshan1319/braj/Image/cheque_image_" + timeStamp + file.getOriginalFilename();
		File newfile = new File(path);
		try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(newfile))) {
			outputStream.write(file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		*/
		 
		 LOGGER.info("File uploaded !!!!");
		
		
		Cheque chequeDataJson = new Cheque();
		LOGGER.info("Save MetaData is in progress.");
		 LOGGER.info("MetaData received ----->"+chequeMetaData.toString());
		
		try {
			
			ObjectMapper objectMapper = new ObjectMapper();
			chequeDataJson = objectMapper.readValue(chequeMetaData, Cheque.class);
			
		}
		catch(IOException err) {
			System.out.printf("Error", err.toString());
		}
		
		LOGGER.info("MetaData Saved !!!!");
		
		return chequeDataJson;
	}
	
	
	public String uploadFile(MultipartFile multipartFile) {
        String fileUrl = "";
        try {
            File file = convertMultiPartToFile(multipartFile);
            String fileName = generateFileName(multipartFile);
            fileUrl = endpointUrl + "/" + bucketName + "/" + fileName;
            uploadFileTos3bucket(fileName, file);
            file.delete();
        } catch (Exception e) {
           e.printStackTrace();
        }
        return fileUrl;
    }

    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    private String generateFileName(MultipartFile multiPart) {
    	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        return timeStamp + "_" + multiPart.getOriginalFilename().replace(" ", "_");
    }

    private void uploadFileTos3bucket(String fileName, File file) {
        s3client.putObject(new PutObjectRequest(bucketName, fileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead));
    }

    public String deleteFileFromS3Bucket(String fileUrl) {
        String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
        s3client.deleteObject(new DeleteObjectRequest(bucketName, fileName));
        return "Successfully deleted";
    }

}
