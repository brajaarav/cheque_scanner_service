package com.bank.chequescanner.test.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.bank.chequescanner.test.model.Cheque;


@Repository
public interface ChequeDataRepository  extends MongoRepository<Cheque, String>  {

}
