package com.bank.chequescanner.test.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.bank.chequescanner.test.model.Cheque;


@Repository
public class ChequeDAOImpl implements ChequeDAO
{
    
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public List<Cheque> getAll() {
		return mongoTemplate.findAll(Cheque.class);
	}

	@Override
	public Cheque getDataById(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, Cheque.class);
	}

	@Override
	public Cheque addNew(Cheque cheque) {
		mongoTemplate.save(cheque);
		// Now, user object will contain the ID as well
		return cheque;
	}
}
