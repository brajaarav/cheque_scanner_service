package com.bank.chequescanner.test.dao;

import java.util.List;

import com.bank.chequescanner.test.model.Cheque;



public interface ChequeDAO 
{
	List<Cheque> getAll();

	Cheque getDataById(String id);

	Cheque addNew(Cheque cheque);
}
