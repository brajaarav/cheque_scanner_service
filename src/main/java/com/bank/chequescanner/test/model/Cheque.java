package com.bank.chequescanner.test.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties (ignoreUnknown = true)

@Document(collection = "database_sequence")
public class Cheque {
    @Id
	private String id;
	private String Date_Of_the_Cheque;
    private String Payee_Name;
    private String Amount_In_Figure;
    private String Amount_In_Words;
    private String Sort_Code;
    private String Bank_Name;
    private String Signature_Availability;
    private String Cheque_Number;
    private String Payers_Account_Number;
    private String Split_Details;
    //private String chequeImage;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDate_Of_the_Cheque() {
		return Date_Of_the_Cheque;
	}
	public void setDate_Of_the_Cheque(String date_Of_the_Cheque) {
		Date_Of_the_Cheque = date_Of_the_Cheque;
	}
	public String getPayee_Name() {
		return Payee_Name;
	}
	public void setPayee_Name(String payee_Name) {
		Payee_Name = payee_Name;
	}
	public String getAmount_In_Figure() {
		return Amount_In_Figure;
	}
	public void setAmount_In_Figure(String amount_In_Figure) {
		Amount_In_Figure = amount_In_Figure;
	}
	public String getAmount_In_Words() {
		return Amount_In_Words;
	}
	public void setAmount_In_Words(String amount_In_Words) {
		Amount_In_Words = amount_In_Words;
	}
	public String getSort_Code() {
		return Sort_Code;
	}
	public void setSort_Code(String sort_Code) {
		Sort_Code = sort_Code;
	}
	public String getBank_Name() {
		return Bank_Name;
	}
	public void setBank_Name(String bank_Name) {
		Bank_Name = bank_Name;
	}
	public String getSignature_Availability() {
		return Signature_Availability;
	}
	public void setSignature_Availability(String signature_Availability) {
		Signature_Availability = signature_Availability;
	}
	public String getCheque_Number() {
		return Cheque_Number;
	}
	public void setCheque_Number(String cheque_Number) {
		Cheque_Number = cheque_Number;
	}
	public String getPayers_Account_Number() {
		return Payers_Account_Number;
	}
	public void setPayers_Account_Number(String payers_Account_Number) {
		Payers_Account_Number = payers_Account_Number;
	}
	public String getSplit_Details() {
		return Split_Details;
	}
	public void setSplit_Details(String split_Details) {
		Split_Details = split_Details;
	}
	
   
}